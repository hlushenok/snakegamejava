package Game;


import javax.swing.*;
import java.awt.event.*;

public class EnterForm extends JFrame{
    private JPanel panel1;
    private JSpinner spinner1;
    private JSpinner spinner2;
    private JButton startButton;
    private JCheckBox checkBox1;
    //Spinners settings
    private SpinnerNumberModel model = new SpinnerNumberModel(150, 50, 250, 10);
    private String[] sizeModels = new String[]{"333x333","666x666","999x999"};
    private SpinnerListModel spinnerSizeModel = new SpinnerListModel(sizeModels);

    public EnterForm(){
        spinner1.setModel(model);
        spinner2.setModel(spinnerSizeModel);
        setVisible(true);
        setSize(300,150);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("Snake options");
        addKeyListener(new KeyboardActions());
        add(panel1);
        checkBox1.setSelected(true);
        startButton.addActionListener(actionEvent -> createGame());
        setLocationRelativeTo(null);
        setFocusable(true);
        pack();
    }

    public void createGame(){
        int fieldSize = 0;
        int snakeSpeed = 0;
        boolean border = false;
        if ((int)spinner1.getValue()>=50&&(int)spinner1.getValue()<=250){
            snakeSpeed = (int)spinner1.getValue();
        }
        if (((String)spinner2.getValue()).equals("333x333")){
            fieldSize = 333;
        }else if (((String)spinner2.getValue()).equals("666x666")){
            fieldSize = 666;
        }else if (((String)spinner2.getValue()).equals("999x999")){
            fieldSize = 999;
        }
        if (checkBox1.isSelected()){
            border = true;
        }

        if (fieldSize == 0 || snakeSpeed == 0){
            dispose();
        }else {
            new MainFrame(fieldSize,snakeSpeed,border);
            dispose();
        }
    }

    class KeyboardActions extends KeyAdapter{
        @Override
        public void keyPressed(KeyEvent keyEvent) {
            super.keyPressed(keyEvent);
            int keyNum = keyEvent.getKeyCode();
            if (keyNum == KeyEvent.VK_ENTER){
                createGame();
            }
        }
    }



}
