package Game;

import Game.Logic.MainActions;

import javax.swing.*;

public class MainFrame extends JFrame {
    public static int FWIDTH ;
    public static  int FHEIGHT;
    public static final String FTITLE = "Snake Game";


    public MainFrame(int size, int speed, boolean border){
        FWIDTH = size;
        FHEIGHT = FWIDTH +35;
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(FWIDTH,FHEIGHT);
        setTitle(FTITLE);
        add(new MainActions(speed, border));
        setLocationRelativeTo(null);
    }
}
