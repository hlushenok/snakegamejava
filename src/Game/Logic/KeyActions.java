package Game.Logic;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyActions extends KeyAdapter {
    @Override
    public void keyPressed(KeyEvent e) {
        super.keyPressed(e);
        int key = e.getKeyCode();
        if(key == KeyEvent.VK_LEFT && !MainActions.right){
            MainActions.left = true;
            MainActions.up = false;
            MainActions.down = false;
        }
        if(key == KeyEvent.VK_RIGHT && !MainActions.left){
            MainActions.right = true;
            MainActions.up = false;
            MainActions.down = false;
        }

        if(key == KeyEvent.VK_UP && !MainActions.down){
            MainActions.right = false;
            MainActions.up = true;
            MainActions.left = false;
        }
        if(key == KeyEvent.VK_DOWN && !MainActions.up){
            MainActions.right = false;
            MainActions.down = true;
            MainActions.left = false;
        }
        if(key == KeyEvent.VK_ENTER && !MainActions.inGame){
            MainActions.inGame = true;
            MainActions.createSnake();
            MainActions.createApple();
        }
}
}
