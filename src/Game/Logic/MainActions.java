package Game.Logic;

import Game.MainFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class MainActions extends JPanel implements ActionListener{
//Main parameters
    private final static int DOT_COLLS = 11;
    private static int FIELD_SIZE = MainFrame.FWIDTH-DOT_COLLS;
    private static int DOT_SIZE = getDotSize(DOT_COLLS);
    private static int ALL_DOTS = (DOT_SIZE*DOT_SIZE);
    private static boolean borders;
    private Timer timer;
    private int timerDelay;
//Snake parameters
    static private int maxSnakeLength;
    static private int snakeLength;
    static private int[] snakeX = new int[ALL_DOTS];
    static private int[] snakeY = new int[ALL_DOTS];
//Apple parameters
    static private int appleX;
    static private int appleY;
//Controllers
    static boolean up;
    static boolean down;
    static boolean left;
    static boolean right;
    private static String lastAction = "";
//Game Status
    static boolean inGame ;
    private static boolean secondStart;
//JPanel init
    public MainActions(int speed,boolean border){
        borders = border;
        timerDelay=speed;
        setBackground(Color.BLACK);
        startGame();
        addKeyListener(new KeyActions());
        setFocusable(true);
    }

    private static int getDotSize(int count){
        int lost = FIELD_SIZE % count;
        int dotSize = FIELD_SIZE / count;
        while (dotSize%count!=0){
            if (lost<=count/2){
                dotSize--;
            }else {
                dotSize++;
            }
        }

        return dotSize;
    }

    private void startGame(){
        timer = new Timer(timerDelay,this);
        timer.start();
    }
//When we create a snake we must refresh "controllers"
     static void createSnake(){
        lastAction = "";
        up = false;
        down = false;
        left = false;
        right = true;
        snakeLength = 3;
        for (int i = 0;i<snakeLength;i++){
            snakeX[i] = DOT_SIZE - i*DOT_SIZE;
            snakeY[i] = DOT_SIZE;
        }
        secondStart = true;
    }
//Here we create apple and check to collisions with snake
    static void createApple(){
        int x = new Random().nextInt(FIELD_SIZE/DOT_SIZE)*DOT_SIZE;
        int y = new Random().nextInt(FIELD_SIZE/DOT_SIZE)*DOT_SIZE;
        boolean create = true;
        for (int i = snakeLength; i > 0; i--){
            if (x == snakeX[i] && y == snakeY[i]){
                create = false;
            }
        }
        if (create) {
            appleX = x;
            appleY = y;
        }else{
            createApple();
        }
    }
//in loop we move's all snake in one position on front
    //and next we turn's a head to some direction
    //we must check it with "lastAction"
    //because we have'nt any synchronization
    //between painting and keyListener
     private void move(){
        for (int i = snakeLength; i > 0; i--) {
                snakeX[i] = snakeX[i - 1];
                snakeY[i] = snakeY[i - 1];
            }

        if (up && !lastAction.equals("down")){
            snakeY[0] -= DOT_SIZE;
            lastAction = "up";
        }
        if (up && lastAction.equals("down")){
            snakeY[0] += DOT_SIZE;
            lastAction = "down";
        }
        if (down && !lastAction.equals("up")){
            snakeY[0] += DOT_SIZE;
            lastAction = "down";
        }
        if (down && lastAction.equals("up")){
            snakeY[0] -= DOT_SIZE;
            lastAction = "up";
        }
        if (left && !lastAction.equals("right")){
            snakeX[0] -= DOT_SIZE;
            lastAction = "left";
        }
        if (left && lastAction.equals("right")){
            snakeX[0] += DOT_SIZE;
            lastAction = "right";
        }
        if (right && !lastAction.equals("left")){
            snakeX[0] += DOT_SIZE;
            lastAction = "right";
        }
        if (right && lastAction.equals("left")){
            snakeX[0] -= DOT_SIZE;
            lastAction = "left";
        }
    }
//Here we check collisions snake with then head
//and choose method for check collisions with border
//or game without borders
    private void checkColision(){
        for (int i = snakeLength; i >0 ; i--) {
            if(i>3 && snakeX[0] == snakeX[i] && snakeY[0] == snakeY[i]){
                inGame = false;
            }
        }if (borders){
            chekBorder();
        }else {
            noBorder();
        }
    }

    private void chekBorder(){
        if(snakeX[0]>=FIELD_SIZE){
            inGame = false;
        }
        if(snakeX[0]<0){
            inGame = false;
        }
        if(snakeY[0]>=FIELD_SIZE){
            inGame = false;
        }
        if(snakeY[0]<0){
            inGame = false;
        }
    }

    private void noBorder(){
        if(snakeX[0]>=FIELD_SIZE){
            snakeX[0]=0;
        }
        if(snakeX[0]<0){
            snakeX[0]= DOT_SIZE*DOT_COLLS;
            System.out.println(DOT_SIZE);
        }
        if(snakeY[0]>= FIELD_SIZE){
            snakeY[0]=0;
        }
        if(snakeY[0]<0){
            snakeY[0] = FIELD_SIZE-DOT_SIZE;
        }
    }
    //If snake head at apple
    private void checkApple(){
        if (snakeY[0]==appleY&&snakeX[0]==appleX){
            snakeLength++;
            createApple();
        }
    }
    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        //Here we draw snake and apple
        if (inGame) {
            graphics.setColor(Color.PINK);
            for (int i = 1; i < snakeLength; i++) {
                graphics.drawOval(snakeX[i], snakeY[i], DOT_SIZE, DOT_SIZE);
            }
            graphics.setColor(Color.MAGENTA);
            graphics.drawOval(snakeX[0], snakeY[0], DOT_SIZE, DOT_SIZE);
            graphics.setColor(Color.GREEN);
            graphics.drawOval(appleX, appleY, DOT_SIZE, DOT_SIZE);

        }else {
            //If first game
            if (!secondStart){
                graphics.drawString("Type Enter to start game",FIELD_SIZE/2-80,FIELD_SIZE/2);
            }else {
                //if player finish
                if (maxSnakeLength<(snakeLength-3)*31){
                    maxSnakeLength=(snakeLength-3)*31;
                }
                graphics.drawString("Top score : "+ maxSnakeLength,FIELD_SIZE/2-37,FIELD_SIZE/2-60);
                graphics.drawString("Your score : "+ (snakeLength-3)*31,FIELD_SIZE/2-40,FIELD_SIZE/2-30);
                graphics.drawString("Type Enter to restart game",FIELD_SIZE/2-80,FIELD_SIZE/2);
            }
        }

    }
//This method calls in loop with timer delay
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (inGame){
            move();
            checkColision();
            checkApple();
        }
        repaint();
    }


}
